import React from 'react'
import Problem from './Problem'
import { observer } from 'mobx-react'

export default observer(({ problem }) => (
  <li>
    <span style={{ textDecoration: (problem.solved ? 'line-through black' : 'inherit') }}>
      <em>{problem.description}</em>
      <br />
      {problem.severity}
    </span>

    <button onClick={() => problem.solved = !problem.solved}>
      {problem.solved ? 'NOT AGAIN' : 'MOVE ON'}
    </button> // sala ang arrow function inside onClick
  </li>
))