// mobx na gid ni
import React, { Component } from 'react';

import { observable, useStrict } from 'mobx'
import { observer, Provider } from 'mobx-react'
import Person from './Person'
import Profile from './Profile';
import PersonStore from './PersonStore'

const personStore = new PersonStore()

useStrict(true)

const Index = observer(props => (
  <Provider personStore={personStore}>
    <div>
      <h1>Hello world

      <Profile />
    </div>
  </Provider>
))

export default Index;