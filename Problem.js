import { observable } from 'mobx'

export default class Problem {
  @observable severity
  @observable description
  @observable solved
  @observable id

  constructor(doc = {}) {
    Object.assign(this, doc);
  }
}