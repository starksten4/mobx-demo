import React, { Component } from 'react';

// DECORATORS - HOW MOBX WORX 
// function Superhero(theClass, member, descriptor) {
function Superhero(TheClass) {
  TheClass.prototype.shootLaser = function() {
    console.log('shooting red beams...')
  }

  TheClass.prototype.fly = function() {
    console.log("up up and away...")
  }

  TheClass.getGroup = function() {
    return 'justice league'
  }
}

function Range(min, max) {
  return function decorate(TheClass, member) {
    return {
      set(newVal) {
        const privateAttrib = '_' + member

        if (newVal >= min && newVal <= max) {
          this[privateAttrib] = newVal
        } else {
          this[privateAttrib] = min
        }
      },

      get() {
        return this['_' + member]
      }
    }
  }
}

let currentCompmutation

class Observable {
  observers = []

  constructor(value) {
    this.data = value
  }

  addObserver(obs) {
    if (!this.observers.includes(obs)) {
      this.observers.push(obs)
    }
  }

  get() {
    this.addObserver(currentCompmutation)
    return this.data
  }

  set(newVal) {
    this.data = newVal
    this.notifyObservers()
  }

  notifyObservers() {
    for (const obs of this.observers) {
      obs()
    }
  }
}

function obs(TheClass, member) {
  return {
    set(newVal) {
      if (this['_' + member]) {
        this['_' + member].set(newVal)
      } else {
        this['_' + member] = new Observable(newVal)
      }
    }, 

    get() {
      return this['_' + member].get()
    }
  }
}

function autorun(func) {
  currentCompmutation = func
  func()
}

// const spellbook = new Observable('Curses and Hexes')

// autorun(() => {
//   console.log('current value of spellbook', spellbook.get())
// })

// setTimeout(() => {
//   spellbook.set('how to shrink your ex')
// }, 5000)

// setTimeout(() => {
//   spellbook.set('Incantations etc.')
// }, 12000)

@Superhero
class Mortal {
  @obs strength
  @obs dexterity
  // @Range(4, 20) strength
  // @Range(50, 70) dexterity
  // _height = 0

  // set height(newHeight) {
  //   this._height = newHeight
  // }

  // get height() {
  //   return this._height
  // }
}

// Superhero(Mortal)

// Object.defineProperties(Mortal.prototype, 'height', {
//   set(newHeight) {
//     this._height = newHeight
//   },
//   get() {
//     return this._height;
//   }
// })

const hero = new Mortal()
hero.strength = 13
hero.dexterity = 55

autorun(() => {
  console.log('hero current stats')
  console.log('str ', hero.strength)
  console.log('dxt ', hero.dexterity)
})

setTimeout(() => {
  hero.strength = 14
}, 5000)

setTimeout(() => {
  hero.dexterity = 5
}, 12000)

class App extends Component {
  render() {
    // const hero = new Mortal()
    // hero.strength = 13
    // hero.dexterity = 55

    // autorun(() => {
    //   console.log('hero current stats')
    //   console.log('str ', hero.strength)
    //   console.log('dxt ', hero.dexterity)
    // })

    // setTimeout(() => {
    //   hero.strength = 14
    // }, 5000)

    // setTimeout(() => {
    //   hero.dexterity = 5
    // }, 12000)

    // console.log('strength', hero.strength)
    // hero.height = 16
    // hero.shootLaser()
    // hero.fly()
    // console.log('group', Mortal.getGroup())
    // console.log('hyt', hero.height)

    return (
      <div>
        Hello world
      </div>
    )
  }
}

export default App;