import { observable, action } from 'mobx' 

export default class Person {
  @observable firstName
  @observable lastName
  @observable score
  @observable problems = [] // my bug ni with object.assign

  constructor(attribs) {
    Object.assign(this, attribs)
  }

  @action.bound
  addProblem(prob) {
    this.problems.push(prob)
  }

  @action.bound
  gainSmallScore() {
    this.score += 5
  }

  // kung arrow function
  // gainHugeScore = action(() => {
  @action.bound
  gainHugeScore() {
    this.score += 40
  }
}